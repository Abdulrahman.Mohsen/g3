struct DataStructure {
   public var text = "Hello, World!"
}
public class TestStack{
   public init() {
        
    }
    public init(firstValue : Int , secondValue : Int){
        print(firstValue + secondValue)
    }
    public init(str : String , ste : String){
        print(str + ste)
    }
    
    public static func printTest(){
        print(#function)
    }
}
public struct stack<Element>{
   public init() {
        
    }
    public init(s : String){
        
    }
    public init(f : Int){
        
    }
   private var storage = [Element]()
    
   public func peek() -> Element?{
        return storage.last
    }
   mutating func push(value : Element){
        storage.append(value)
    }
    @discardableResult
    mutating func pop() -> Element?{
        storage.popLast()
    }
}
